package Pakaje;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CasosProvaSH {
	
	 private String[][] inicialitzarImprimir(int f, int c, int fs, int cs, int fh, int ch) {
	        String[][] m = Ejercicio.inicialitzarmatriu(f, c);
	        Ejercicio.omplirmatriu(m, fs, cs, fh, ch);
	        Ejercicio.imprimirmatriu(m);
	        return m; //mira que la matriu estigui ben incialitzada i impressa
	    }

	    private boolean compararMatrius(String[][] matriuEsperada, String[][] matriuGenerada) {
	        if (matriuEsperada.length != matriuGenerada.length || matriuEsperada[0].length != matriuGenerada[0].length) {
	            return false; //compara la matriu i mira que el tamany sigui el correcte
	        }

	        for (int i = 0; i < matriuEsperada.length; i++) {
	            for (int j = 0; j < matriuEsperada[0].length; j++) {
	                if (!matriuEsperada[i][j].equals(matriuGenerada[i][j])) {
	                    return false; //si troba alguna cosa diferent retorna false
	                }
	            }
	        }

	        return true;
	    }

	@Test
    public void test() {
        String[][] matriuesperada = {
        		  {"X", "X", "X", "X", "X"},
                  {"X", "X", "X", "X", "X"},
                  {"X", "X", "H", "X", "X"},
                  {"X", "X", "X", "X", "X"},
                  {"X", "X", "X", "X", "X"}
        };

        String[][] matriugenerada = inicialitzarImprimir(5, 5, 2, 2, 2, 2);
        int distanciaTotal=Ejercicio.calculardistancia(2, 2, 2, 2);
        
        String resultat=Ejercicio.determinartemperatura(distanciaTotal);
       
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(0, distanciaTotal);
        assertEquals("HAS TROBAT L'AIGUA", resultat);
    }

	@Test
	public void test2() {
		 String[][] matriuesperada = {
       		  	 {"X", "X", "X", "X", "X"},
                 {"X", "X", "X", "X", "X"},
                 {"X", "X", "S", "X", "X"},
                 {"X", "X", "H", "X", "X"},
                 {"X", "X", "X", "X", "X"}
       };
	    String[][] matriugenerada = inicialitzarImprimir(5, 5, 2, 2, 3, 2);
		int distanciaTotal=Ejercicio.calculardistancia(2,2, 3,2);
		String resultat = Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(1, distanciaTotal);
		assertEquals("MOLT CALENT", resultat);
		
	}
	@Test
	public void test3() {
		 String[][] matriuesperada = {
	       		  	 {"X", "S", "X", "X", "X"},
	                 {"X", "X", "H", "X", "X"},
	                 {"X", "X", "X", "X", "X"},
	                 {"X", "X", "X", "X", "X"},
	                 {"X", "X", "X", "X", "X"}
	       };
		String[][]matriugenerada=inicialitzarImprimir(5, 5, 0, 1, 1, 2);
		int distanciaTotal = Ejercicio.calculardistancia(0, 1, 1, 2);
		String resultat = Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(2, distanciaTotal);
		assertEquals("CALENT", resultat);
	}
	@Test
	public void test4() {
		String[][] matriuesperada = {
       		  	{"X", "X", "X", "X", "X"},
                 {"X", "X", "X", "X", "X"},
                 {"X", "H", "X", "X", "X"},
                 {"X", "X", "X", "X", "X"},
                 {"S", "X", "X", "X", "X"}
       };
		String[][]matriugenerada=inicialitzarImprimir(5, 5, 4, 0, 2, 1);
		int distanciaTotal = Ejercicio.calculardistancia(4, 0, 2, 1);
		String resultat = Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(3, distanciaTotal);
		assertEquals("FRED", resultat);
	}
	@Test
	public void test5() {
		String[][] matriuesperada = {
       		  	 {"X", "X", "X", "X", "X"},
                 {"X", "X", "X", "X", "X"},
                 {"X", "X", "X", "S", "X"},
                 {"X", "X", "X", "X", "X"},
                 {"X", "H", "X", "X", "X"}
       };
		String[][]matriugenerada=inicialitzarImprimir(5, 5, 2, 3, 4, 1);
		int distanciaTotal=Ejercicio.calculardistancia(2, 3, 4, 1);
		String resultat=Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(4, distanciaTotal);
		assertEquals("FRED", resultat);

	}
	@Test
	public void test6() {
		String[][] matriuesperada = {
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "H", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "S", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      };
		String[][]matriugenerada=inicialitzarImprimir(10, 10, 2, 1, 1, 1);
		int distanciaTotal=Ejercicio.calculardistancia(2, 1, 1, 1);
		String resultat=Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(1, distanciaTotal);
		assertEquals("MOLT CALENT", resultat);
	}
	@Test
	public void test7() {
		String[][] matriuesperada = {
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "S", "H", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      };
			String[][]matriugenerada=inicialitzarImprimir(10, 10, 5, 5, 5, 6);
			int distanciaTotal=Ejercicio.calculardistancia(5, 5, 5, 6);
			String resultat=Ejercicio.determinartemperatura(distanciaTotal);
	        assertTrue(compararMatrius(matriuesperada, matriugenerada));
	        assertEquals(1, distanciaTotal);
			assertEquals("MOLT CALENT", resultat);
		}
	@Test
    public void test8() {
        String[][] matriuesperada = {
        		  {"S", "X", "X", "X", "X"},
                  {"X", "X", "X", "X", "X"},
                  {"X", "X", "X", "X", "X"},
                  {"X", "X", "X", "X", "X"},
                  {"X", "X", "X", "X", "H"}
        };

        String[][] matriugenerada = inicialitzarImprimir(5, 5, 0, 0, 4, 4);
        int distanciaTotal=Ejercicio.calculardistancia(0, 0, 4, 4);
        
        String resultat=Ejercicio.determinartemperatura(distanciaTotal);
       
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(8, distanciaTotal);
        assertEquals("FRED", resultat);
    }

   
    
    @Test
    public void Test9() {
    String[][] matriuesperada = {
  		  	 {"X", "X", "X", "X", "X", "X"},
  		  	 {"X", "X", "X", "X", "X", "X"},
  		  	 {"X", "X", "X", "X", "X", "X"},
  		  	 {"X", "X", "X", "X", "X", "H"},
  		  	 {"X", "X", "X", "X", "X", "X"},
  		  	 {"X", "X", "S", "X", "X", "X"},
  };
	String[][]matriugenerada=inicialitzarImprimir(6, 6, 5, 2, 3, 5);
	int distanciaTotal=Ejercicio.calculardistancia(5, 2, 3, 5);
	String resultat=Ejercicio.determinartemperatura(distanciaTotal);
	assertTrue(compararMatrius(matriuesperada, matriugenerada));
    assertEquals(5, distanciaTotal);
	assertEquals("FRED", resultat);

    }
    @Test
    public void Test10() {
    	String[][] matriuesperada = {
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "H", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
    			  {"S", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
     	
     };
    	String[][]matriugenerada=inicialitzarImprimir(20, 20, 19, 0, 3, 17);
    	int distanciaTotal=Ejercicio.calculardistancia(19, 0, 3, 17);
    	String resultat=Ejercicio.determinartemperatura(distanciaTotal);
    	assertTrue(compararMatrius(matriuesperada, matriugenerada));
    	assertEquals(33, distanciaTotal);
    	assertEquals("FRED", resultat);
    	
    }
    @Test
    public void Test11(){
    String[][] matriuesperada = {
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "S", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "H", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      };
		String[][]matriugenerada=inicialitzarImprimir(10, 10, 5, 7, 6, 6);
		int distanciaTotal=Ejercicio.calculardistancia(5, 7, 6, 6);
		String resultat=Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(2, distanciaTotal);
		assertEquals("CALENT", resultat);
	}
    
    @Test
    public void Test12(){
    String[][] matriuesperada = {
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "S", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "H", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      };
		String[][]matriugenerada=inicialitzarImprimir(10, 10, 3, 4, 7, 2);
		int distanciaTotal=Ejercicio.calculardistancia(3, 4, 7, 2);
		String resultat=Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(6, distanciaTotal);
		assertEquals("FRED", resultat);
	}
    
    @Test
    public void Test13(){
    String[][] matriuesperada = {
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "H", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "S", "X", "X", "X", "X", "X", "X", "X"},
      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
      };
		String[][]matriugenerada=inicialitzarImprimir(10, 10, 8, 2, 7, 3);
		int distanciaTotal=Ejercicio.calculardistancia(8, 2, 7, 3);
		String resultat=Ejercicio.determinartemperatura(distanciaTotal);
        assertTrue(compararMatrius(matriuesperada, matriugenerada));
        assertEquals(2, distanciaTotal);
		assertEquals("CALENT", resultat);
	}
    
    @Test
	public void test13() {
		String[][] matriuesperada = {
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "H", "S", "X", "X"},
	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
	      };
			String[][]matriugenerada=inicialitzarImprimir(10, 10, 8, 7, 8, 6);
			int distanciaTotal=Ejercicio.calculardistancia(8, 7, 8, 6);
			String resultat=Ejercicio.determinartemperatura(distanciaTotal);
	        assertTrue(compararMatrius(matriuesperada, matriugenerada));
	        assertEquals(1, distanciaTotal);
			assertEquals("MOLT CALENT", resultat);
		}
    
    @Test
   	public void test14() {
   		String[][] matriuesperada = {
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "S", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "H", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      };
   			String[][]matriugenerada=inicialitzarImprimir(10, 10, 6, 5, 7, 4);
   			int distanciaTotal=Ejercicio.calculardistancia(6, 5, 7, 4);
   			String resultat=Ejercicio.determinartemperatura(distanciaTotal);
   	        assertTrue(compararMatrius(matriuesperada, matriugenerada));
   	        assertEquals(2, distanciaTotal);
   			assertEquals("CALENT", resultat);
   		}
    
    @Test
   	public void test15() {
   		String[][] matriuesperada = {
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "X", "X", "X", "X", "X", "X", "X", "X", "X"},
   	      		{"X", "S", "X", "H", "X", "X", "X", "X", "X", "X"},
   	      };
   			String[][]matriugenerada=inicialitzarImprimir(10, 10, 9, 1, 9, 3);
   			int distanciaTotal=Ejercicio.calculardistancia(9, 1, 9, 3);
   			String resultat=Ejercicio.determinartemperatura(distanciaTotal);
   	        assertTrue(compararMatrius(matriuesperada, matriugenerada));
   	        assertEquals(2, distanciaTotal);
   			assertEquals("CALENT", resultat);
   		}
}
    

