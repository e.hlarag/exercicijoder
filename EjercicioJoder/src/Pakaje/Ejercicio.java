package Pakaje;

import java.util.Scanner;

/**
 * @author Hector Lara Garcia.
 * @version 1.0.
 * @since 14/01/2024.
 * 
 * Aquesta classe representa un joc en un tauler bidimensional amb una sortida
 * (S) i un destí (H). El programa determina la temperatura del destí en base a
 * la distancia entre la sortida i el destí.
 */
public class Ejercicio {
	/**
	 * El mètode principal que executa el joc i determina la temperatura.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//Sol·licitar fila i columna a l'usuari.
		int f = sc.nextInt();
		int c = sc.nextInt();
		String[][] m = inicialitzarmatriu(f, c);
		
		//Sol·licitar les coordenades de S i H
		int fs = sc.nextInt();
		int cs = sc.nextInt();
		int fh = sc.nextInt();
		int ch = sc.nextInt();
		
		omplirmatriu(m, fs, cs, fh, ch);
		imprimirmatriu(m);
		
		int distanciaTotal = calculardistancia(fs, cs, fh, ch);
		determinartemperatura(distanciaTotal);
		
		
	}
	
	/**
	 * Mètode que determina la temperatura segons la distància entre S i H i mostra un missatge per consola.
	 * 
	 * @param distanciaTotal	La distància total entre S i H.
	 * @return Un missatge indicant la temperatura.
	 */
	public static String determinartemperatura(int distanciaTotal) {
		
		System.out.println("Distancia: "+ distanciaTotal);
		if (distanciaTotal == 0) {
			System.out.println("HAS TROBAT L'AIGUA");
			return "HAS TROBAT L'AIGUA";
		} else if (distanciaTotal == 1) {
			System.out.println("MOLT CALENT");
			return "MOLT CALENT";
		} else if (distanciaTotal == 2) {
			System.out.println("CALENT");
			return "CALENT";
		} else {
			System.out.println("FRED");
			return "FRED";
		}
	}
	/**
	 * Operacions per calcular la distància vertical i horitzontal entre S i H.
	 * Primer restem les files i en cas que el total sigui inferior a 0 li cambiem el valor, així mai 
	 * hi hauran negatius quan H és a sota de S i ens assegurem que es calculi bé la distància. La següent operació és
	 * exactament igual, però en comptes de files, amb columnes. Per últim sumem les
	 * dues distàncies, obtenint d'aquesta manera la distància total.
	 * 
	 * @param fs Fila l'ampolla.
     * @param cs Columna de l'ampolla.
     * @param fh Fila de l'Héctor.
     * @param ch Columna de l'Héctor
     * 
     * @return La distancia total entre l'ampolla i l'Héctor.
	 */
	public static int calculardistancia(int fs, int cs, int fh, int ch) {
		
		int distanciaFila = fs - fh;
		if (distanciaFila < 0) {
			distanciaFila = -distanciaFila;
		}
		int distanciaColumna = cs - ch;
		if (distanciaColumna < 0) {
			distanciaColumna = -distanciaColumna;
		}
		return distanciaFila + distanciaColumna;
	}
	
	/**
     * Mètode per a imprimir la matriu per la consola.
     *
     * @param m Matriu a imprimir.
     */
	public static void imprimirmatriu(String[][] m) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[0].length; j++) {
				System.out.print(m[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/**
	 * Mètode per representar les cordenades FS,CS com a S i FH,CH com a H
	 * 
	 * @param m  Matriu a omplir.
     * @param fs Fila de l'ampolla.
     * @param cs Columna de l'ampolla.
     * @param fh Fila de l'Héctor.
     * @param ch Columna de l'Héctor.
	 */
	public static void omplirmatriu(String[][] m, int fs, int cs, int fh, int ch) {
		m[fs][cs] = "S";
		m[fh][ch] = "H";
	}

	/**
	 * Mètode per omplir la matriu amb el caràcter "X"
	 * 
	 * @param f Número de files de la matriu.
	 * @param c Número de columnes de la matriu.
	 * @return La matriu inicialitzada.
	 */
	public static String[][] inicialitzarmatriu(int f, int c) {
		String[][] m = new String[f][c];
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[0].length; j++) {
				m[i][j] = "X";
			}
		}
		return m;
	}

}
